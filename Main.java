import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ProductManager productManager = new ProductManager();
        int number;

        while (true) {
            System.out.println("Press 1 to add a product.\n" +
                    "Press 2 to sort by product ID.\n" +
                    "Press 3 to sort by product name.\n" +
                    "Press 4 to sort by product price.\n" +
                    "Press 5 to display all products.\n" +
                    "Press 0 to exit\n");

             number = sc.nextInt();

            switch (number) {

                case 1:
                    int id, price;
                    String productName;
                    System.out.println("Введите ID товара");
                    id = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Введите имя товара");
                    productName = sc.nextLine();
                    System.out.println("Введите цену товара");
                    price = sc.nextInt();
                    productManager.add(new Product(id, productName, price));
                    break;
                case 2:
                    productManager.sortProducts();
                    break;
                case 3:
                    productManager.sortByName();
                    break;
                case 4:
                    productManager.sortByPrice();
                    break;
                case 5:
                    productManager.showProducts();
                    System.out.println();
                    break;
                case 0:
                    break;

                default:
                    System.out.println("Вы ввели неправильное значение, попробуйте ещё раз");
                    System.out.println();
                    break;
            }

            if (number == 0)
                break;


        }
    }
}
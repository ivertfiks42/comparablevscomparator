import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductManager {
    private List<Product> productList = new ArrayList<>();

    public void add(Product product){
        productList.add(product);
    }

    public void showProducts(){
        for(Product product : productList){
            System.out.println(product.toString());
        }
    }

    public void sortProducts(){
        Collections.sort(productList);
    }

    public void sortByName(){
        Collections.sort(productList, new ProductNameComparator());
    }

    public void sortByPrice(){
        Collections.sort(productList, new ProductPriceComparator());
    }
}